function getXPathForElement(element) {
  const idx = (sib, name) => sib
    ? idx(sib.previousElementSibling, name||sib.localName) + (sib.localName == name)
    : 1;
  const segs = elm => !elm || elm.nodeType !== 1
    ? ['']
    : elm.id && document.getElementById(elm.id) === elm
      ? [`id("${elm.id}")`]
      : [...segs(elm.parentNode), `${elm.localName.toLowerCase()}[${idx(elm)}]`];
  return segs(element).join('/');
}

function setHoverListener() {

  $('body > *').off('mouseover mouseout');

  $('body > *').on('mouseover mouseout', function(e) {
    $(this).toggleClass('hovering', e.type === 'mouseover');

    if (e.type === 'mouseover') {
      const domRect = this.getBoundingClientRect();
      const el = document.createElement('span');
      el.setAttribute('id', 'hover-label');
      el.setAttribute('class', 'hovering-label');
      el.setAttribute('style', `left: ${domRect.left}px; top: ${domRect.top - 20}px;`);
      el.innerText = this.tagName;
      $('body').append(el);
    } else if (e.type === 'mouseout') {
      $('#hover-label').remove();
    }
    e.stopPropagation();
  });
}


$(document).ready(() => {

  $('body').on('documented_updated', () => {
    console.log('documented updated');
    setHoverListener();
  });

  setHoverListener();

  $('body').click((event) => {
    const el = document.elementFromPoint(event.pageX, event.pageY);
    const s = getXPathForElement(el);
    parent.$('body').trigger('iframeclick', {pageX: event.pageX, pageY: event.pageY, element: el, selector: s});
  });
});

