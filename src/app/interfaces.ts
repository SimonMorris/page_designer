import { EOrientation } from './enums';

export interface IPageDesignerTemplateCategory {
  expanded?: boolean;
  title: string;
  templates: IPageDesignerTemplate[];
}

export interface IPageDesignerTemplate {
  id: number;
  title: string;
  template: string;
}

export interface IPageDesignerState {
  draggingTemplate: any;
  panels: IPageDesignerPanelState;
  iframeDOMRect: DOMRect;
  mouse: IPageDesignerMouseState;
  highlightedElement: Element;
  highlightedElementXPath: string;
  zoomLevel: number;
  scale: ICanvasScale;
  rotate: boolean;
}

interface IPageDesignerPanelState {
  bottomPanelExpanded: boolean;
  leftPanelExpanded: boolean;
  rightPanelExpanded: boolean;
}

interface IPageDesignerMouseState {
  x: number;
  y: number;
}

export interface ICanvasScaleObj {
  [label: string]: ICanvasScale;
}

export interface ICanvasScale {
  label: string;
  width: number;
  height: number;
}

export interface State {
  viewer: IViewerState;
}

export interface IViewerState {
  zoomOptionIdx: number;
  scale: ICanvasScale;
  orientation: EOrientation;
}

export interface IDragData {
  target: HTMLElement;
  dropData: IDropData;
}

export interface IDropData {
  dropFn?: () => void;
  className?: string;
  top?: number;
  left?: number;
  width?: number;
  height?: number;
}
