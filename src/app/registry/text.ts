import { IPageDesignerTemplate } from '../interfaces';

export let HeadingTemplate: IPageDesignerTemplate = {
  id: 6,
  title: 'Heading',
  template: '<h1>Heading</h1>'
};

export let ParagraphTemplate: IPageDesignerTemplate = {
  id: 7,
  title: 'Paragraph',
  template: '<p>Paragraph</p>'
};

export let TextTemplates = [HeadingTemplate, ParagraphTemplate];
