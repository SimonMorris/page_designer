import { IPageDesignerTemplate } from '../interfaces';

export let ContainerTemplate: IPageDesignerTemplate = {
  id: 8,
  title: 'Container',
  template: '<div data-template="Container" class="container"></div>'
};

export let RowTemplate: IPageDesignerTemplate = {
  id: 9,
  title: 'Row',
  template: '<div data-template="Row" class="row"></div>'
};

export let ColumnTemplate: IPageDesignerTemplate = {
  id: 10,
  title: 'Column',
  template: '<div data-template="Column" class="col-md-12"></div>'
};

export let PageStructureTemplates = [ContainerTemplate, RowTemplate, ColumnTemplate];
