import { ETemplateGroup } from '../enums';

export const TEMPLATES = [
  {
    title: 'Alert',
    group: ETemplateGroup.TEXT,
    template: '<div class="alert alert-primary" role="alert">\n' +
      '  This is a primary alert—check it out!\n' +
      '</div>',
  },
  {
    title: 'Blockquote',
    group: ETemplateGroup.TEXT,
    template: '<blockquote class="blockquote"><p class="mb-0">Lorem ipsum dolor sit amet</p>' +
      '<footer class="blockquote-footer">Someone famous</footer></blockquote>'
  },
  {
    title: 'Bold',
    group: ETemplateGroup.TEXT,
    template: '<b>Bold Text</b>'
  },
  {
    title: 'Button',
    group: ETemplateGroup.COMPONENT,
    template: '<button type="button" class="btn btn-primary">Button</button>'
  },
  {
    title: 'Button Group',
    group: ETemplateGroup.COMPONENT,
    template: '<div class="btn-group" role="group" aria-label="Basic example">\n' +
      '  <button type="button" class="btn btn-primary">Left</button>\n' +
      '  <button type="button" class="btn btn-primary">Middle</button>\n' +
      '  <button type="button" class="btn btn-primary">Right</button>\n' +
      '</div>'
  },
  {
    title: 'Card',
    group: ETemplateGroup.COMPONENT,
    template: '<div class="card" style="width: 18rem;">\n' +
      '  <img src="..." class="card-img-top" alt="...">\n' +
      '  <div class="card-body">\n' +
      '    <h5 class="card-title">Card title</h5>\n' +
      '    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card\'s content.</p>\n' +
      '    <a href="#" class="btn btn-primary">Go somewhere</a>\n' +
      '  </div>\n' +
      '</div>'
  },
  {
    title: 'Column',
    group: ETemplateGroup.PAGE_STRUCTURE,
    template: '<div data-template="Column" class="col-md-12"></div>'
  },
  {
    title: 'Container',
    group: ETemplateGroup.PAGE_STRUCTURE,
    template: '<div data-template="Container" class="container"></div>'
  },
  {
    title: 'Heading',
    group: ETemplateGroup.TEXT,
    template: '<h1>Heading</h1>'
  },
  {
    title: 'Italic',
    group: ETemplateGroup.TEXT,
    template: '<i>Italic text</i>'
  },
  {
    title: 'Image',
    group: ETemplateGroup.MEDIA,
    template: '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAyAAAAJYBAMAAABoWJ9DAAAALVBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADBoCg+AAAAD3RSTlMAXgYsRRgTViRKHAw8UTLfejSWAAAG1UlEQVR42uzBgQAAAACAoP2pF6kCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGD27V8nqiAKwPjJggi7hOQIAosswT+FhSRa2kHiA7CVLVhQWLkxoZCG7WiJdjYkvsB2tKyllcsTQOdjaBaVuXPuXkHu3BnN93uFL7lzMnMPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMD1IBZBjicDjaV5JPA1BhrPrMA3rjFtCjzbGtOJwNPVmL4KkgryRkCQtBEkMQRJzDDIhxfV2yDI6CBHUr0pghDkX0CQxBAkMQRJDEESQ5DEECQxBEkMQRJDkMQQJDEESQxBEkOQxOQEaezsSA6CVMEGmXiuqq0vYhCkAjZI/VCH3q6KhyDh2SCNDf3pnXgIEp4N0tXfPksWQYKzQab1UqsnGQQJzgbpq+NUsggSmA0ypq6mZBAkNBukrRln4iJIaDbIQDPuSgZBwrJBxjSrKS6CBGaD3C5ccCJIYDZIt3DBiSCB2SAb6rkjDoIEZoOob0kcBAnLBplQ34o4CBKWDTKpvjlxECQsG2RGfS1xECRXpUEWxEGQXAT5z/HJSgyHemKcILfUtywOgoRlg9TUtygOgoRlg8iheu6JiyAFqrlcXBcHQQKzQe6rZ18cBAnMBpm50dRb+/StR5BSg9Q6N7jsrZ2rNo8IUmYQ6d7gferlcE7uEaTMINPqaq3K1dV1aJcgZQWxb4a7cg1tvbBFkDKDPNJLC1tydY2OXtgjSDlB7L+kH687xA41CVJqkHrn755vz/WXM4KUGUQeDovYealY3ZmVCVJqEHnc1x/e9+Q6jp2jp0eQkpc+Xx8cvJKrstPZOkGir0XPqGOFINGDtNW1SZDIQRodde0RJHKQcc2YI0jkIM806ylBogaZUM88QaIGWVNPa5UgMYP01XdCkIhBJtVYJEjEINtqbREkWpDaQK1TgkQLMq05lgkSLUhX8+wTJFKQRkfz7BEkUpApzdUkSKQg55rvjCBRgtR1hCWCRAlyrCMs9AgSI8iGjrJOkAhBxnSkWYJECNLW0TYJUmEQs79gnRKk8iDTWmCZIJUH6WqRfYJUHGRCC80TpOIga1qotUqQaoP0tdgJQSoNMql/sEiQSoN8Z++OVdSIojCOg6viqggfuuvqumKXStAiveYJtEiXgDaBkMq0NvoGTp9AfAOtbPUR9AmybxLYpEsOc++dc25c+P7tTPerLt8dZo+0dgQxAPnw/du/nydIa0MQfZDPAFoT4cp7Sl2CqIOUxUPeFulNCKINssZL74TtNqWvBFEGKeB3rZ683UKuQRBlkIN4phjCpQFBdEESad4owKk7gqiC1MTboQc41ewRRBNkKs4bY7g1IogiSGUhzRs3cKxOEEWQvDhvzOHajCB6IENp3sid4dqFIGogRXHeqMK5DkHUQPrivLGGeyuCaIGcpHmjCI8eCaIEUhLnjT48ahFECWQuzhsn+HQkiApI7izNGyV41SaICkgVf/XnvQO8au4IogGyFj9US+DXhiAKIJWFNG/U4FmXIAogt+K8sYVvE4JkB3mW5o3KAr4tCZIZpCzOG3l41yBIZpCDeKYYwr8BQbKCjKV5o4CA7gmSEeQGQrM+AnrqESQbyBRClxNCOhIkE4g8QLUQ1ANBMoFUod2MIFlA1tDuQpAMIEWo1yFIBpA+9FsRJBzkBP2WBAkGKcGgFkGCQfaw6CdBQkESWNQmSCBIDSY1dwQJA9nCphFBgkAqC9hUJ0gQSB5WTQgSAjKEVReCBIAUYFaHIAEgb2DXiiD+IGPYdU8Qb5AbGPbUI4gvyByWHQniCZI7w7IHgniCVGHbjiB+IGvYtiGIF0gFxnUJ4gVyC+t+EMQH5BnWLQniAVKGeQ2CeIAcYN+AIO4gCey7I4gzSA0RavYI4goyRYxGBHEEyS0QozpBHEHyiNOMIG4gQ8TpQhAnkCIi1SGIE0gfsVoRxAXkhFg9EsQBpIRotQjiALJHvI4ESQdJEK82QVJBcp8i9oUgaSBW8f8hBHnlEeTKIsiVRZAr6wXk7fv4fSSIAPK/IghBfrFHxzQAAjEAAJswMlUACUggbNjDABbQxYIU9kdAn+TOwv2BkM4I6YyQzlxZ6QwaS1Z6gsaQhaY5aK1Z5w4+xmOrsgcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALztwSEBAAAAgKD/r51hAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgFJxAtJ60EBzsAAAAAElFTkSuQmCC\n" alt=""/>'
  },
  {
    title: 'Navigation Header',
    group: ETemplateGroup.CONTENT,
    template: '<ul class="nav justify-content-center grey lighten-4 py-4">\n' +
      '  <li class="nav-item">\n' +
      '    <a class="nav-link active" href="#!">Active</a>\n' +
      '  </li>\n' +
      '  <li class="nav-item">\n' +
      '    <a class="nav-link" href="#!">Link</a>\n' +
      '  </li>\n' +
      '  <li class="nav-item">\n' +
      '    <a class="nav-link" href="#!">Link</a>\n' +
      '  </li>\n' +
      '  <li class="nav-item">\n' +
      '    <a class="nav-link disabled" href="#!">Disabled</a>\n' +
      '  </li>\n' +
      '</ul>'

  },
  {
    title: 'One Columns [12]',
    group: ETemplateGroup.LAYOUT,
    template: '<div data-template="Container" class="container">' +
      '<div data-template="Row" class="row">' +
      '<div data-template="Column" class="col-md-12"></div>' +
      '</div></div></div>'
  },
  {
    title: 'Paragraph',
    group: ETemplateGroup.TEXT,
    template: '<p>Paragraph</p>'
  },
  {
    title: 'Row',
    group: ETemplateGroup.PAGE_STRUCTURE,
    template: '<div data-template="Row" class="row"></div>'
  },
  {
    title: 'Span',
    group: ETemplateGroup.TEXT,
    template: '<span>Span of text</span>'
  },
  {
    title: 'Small',
    group: ETemplateGroup.TEXT,
    template: '<small>Small Text</small>'
  },
  {
    title: 'Table',
    group: ETemplateGroup.TABLE,
    template: '<div class="table-responsive">' +
      '<table class="table"><thead><tr><th>Column 1</th><th>Column 2</th></tr></thead>' +
      '<tbody><tr><td>Cell 1</td><td>Cell 2</td></tr>' +
      '<tr><td>Cell 1</td><td>Cell 2</td></tr></tbody>' +
      '</table></div>'
  },
  {
    title: 'Table Row',
    group: ETemplateGroup.TABLE,
    template: '<tr></tr>'
  },
  {
    title: 'Table Cell',
    group: ETemplateGroup.TABLE,
    template: '<td></td>'
  },
  {
    title: 'Two Columns [6-6]',
    group: ETemplateGroup.LAYOUT,
    template: '<div data-template="Container" class="container">' +
      '<div data-template="Row" class="row">' +
      '<div data-template="Column" class="col-md-6"></div>' +
      '<div data-template="Column" class="col-md-6">' +
      '</div></div></div>'
  },
  {
    title: 'Two Columns [3-9]',
    group: ETemplateGroup.LAYOUT,
    template: '<div data-template="Container" class="container">' +
      '<div data-template="Row" class="row">' +
      '<div data-template="Column" class="col-md-3"></div>' +
      '<div data-template="Column" class="col-md-9">' +
      '</div></div></div>'
  }
];

const labelMap = {
  [ETemplateGroup.CONTENT]: 'Content',
  [ETemplateGroup.LAYOUT]: 'Layouts',
  [ETemplateGroup.PAGE_STRUCTURE]: 'Page Structure',
  [ETemplateGroup.COMPONENT]: 'Components',
  [ETemplateGroup.TEXT]: 'Text',
  [ETemplateGroup.MEDIA]: 'Media',
  [ETemplateGroup.TABLE]: 'Table'
};

export const TEMPLATE_GROUPS = (() => {
  const groupMap = {};
  TEMPLATES.reduce((map, t) => {
    if (!map[t.group]) {
      map[t.group] = [];
    }
    map[t.group].push(t);
    return map;
  }, groupMap);

  return Object.keys(groupMap).map(group => ({
    group,
    expanded: false,
    groupName: labelMap[group],
    templates: groupMap[group]
  }));
})();
