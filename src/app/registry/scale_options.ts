import { ICanvasScaleObj } from '../interfaces';

export const ScaleOptions: ICanvasScaleObj = {
  'desktop-hd': { width: 1366, height: 768, label: 'Desktop HD (1366px x 768px)' },
  'google-pixel4': { width: 411, height: 869, label: 'Google Pixel 4  (411px x 869px)' },
  'google-pixel4-xl': { width: 411, height: 869, label: 'Google Pixel 4 XL (411px x 869px)' },
  'iphone-xs': { width: 375, height: 812, label: 'iPhone XS (375px x 812px)' },
  'iphone-se': { width: 375, height: 667, label: 'iPhone SE (375px x 667px)' },
  'iphone-11': { width: 414, height: 896, label: 'iPhone 11 (414px x 869px)' },
  'iphone-11-pro': { width: 375, height: 812, label: 'iPhone 11 Pro (375px x 812px)' },
  'iphone-11-pro-max': { width: 414, height: 896, label: 'iPhone 11 Pro Max (414px x 896px)' },
  'ipad-original': { width: 768, height: 1024, label: 'iPad (768px x 1024px)' },
  'ipad-pro4': { width: 1024, height: 1366, label: 'iPad Pro 4 (1024px x 1366px)' },
  'macbook-air': { width: 1280, height: 800, label: 'Macbook Air (1280px x 800px)' },
  'macbook-pro-13': { width: 1280, height: 800, label: 'Macbook Pro 13inch (1280px x 800px)' },
  'macbook-pro-15': { width: 1440, height: 900, label: 'Macbook Pro 15inch (1440px x 900px)' },
  'ms-surface-pro-7': { width: 1368, height: 912, label: 'Microsoft Surface Pro 7 (1368px x 912px)' },
  'chromebook-pixel': { width: 1280, height: 850, label: 'Chromebook Pixel (1280px x 850px)' }
};

export const DEFAULT_SCAlE_OPTION_KEY = 'desktop-hd';
