import { IPageDesignerTemplate } from '../interfaces';

export let AlertTemplate: IPageDesignerTemplate = {
  id: 1,
  title: 'Alert',
  template: '<div class="alert alert-primary" role="alert">\n' +
    '  This is a primary alert—check it out!\n' +
    '</div>'
};

export let ButtonTemplate: IPageDesignerTemplate = {
  id: 2,
  title: 'Button',
  template: '<button type="button" class="btn btn-primary">Button</button>'
};

export let ButtonGroupTemplate: IPageDesignerTemplate = {
  id: 3,
  title: 'Button Group',
  template: '<div class="btn-group" role="group" aria-label="Basic example">\n' +
    '  <button type="button" class="btn btn-primary">Left</button>\n' +
    '  <button type="button" class="btn btn-primary">Middle</button>\n' +
    '  <button type="button" class="btn btn-primary">Right</button>\n' +
    '</div>'
};

export let CardTemplate: IPageDesignerTemplate = {
  id: 4,
  title: 'Card',
  template: '<div class="card" style="width: 18rem;">\n' +
    '  <img src="..." class="card-img-top" alt="...">\n' +
    '  <div class="card-body">\n' +
    '    <h5 class="card-title">Card title</h5>\n' +
    '    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card\'s content.</p>\n' +
    '    <a href="#" class="btn btn-primary">Go somewhere</a>\n' +
    '  </div>\n' +
    '</div>'
};

export let ComponentTemplates = [
  AlertTemplate,
  ButtonTemplate,
  ButtonGroupTemplate,
  CardTemplate
];
