import { EEventTarget } from './enums';

export const isDefined = (value) => value !== undefined;

export const clamp = (min: number, max: number) =>
  (value: number): number =>
    value < min ? min : value > max ? max : value;

export const dispatchCustomEvent = <T>(eventName: string, detail: T, _window?: EEventTarget): void => {
  const evt = new CustomEvent<T>(eventName, { detail });
  switch (_window) {
    case EEventTarget.FRAME:
      const frame = window.frames[0];
      if (frame) { frame.dispatchEvent(evt); }
      break;
    case EEventTarget.HOST:
      window.top.dispatchEvent(evt);
      break;
    default:
      window.dispatchEvent(evt);
  }
};

export const getBoxParams = (target: HTMLElement, zoom: number) => {
  if (!target) {
    return null;
  }
  const rect = target.getBoundingClientRect();
  return {
    top: rect.top * zoom,
    left: rect.left * zoom,
    width: rect.width * zoom,
    height: rect.height * zoom,
    tagName: target.id === 'body' ? 'body' : target.tagName
  };
};

export const htmlToElement = (html: string): Element => {
  const template = document.createElement('template');
  html = html.trim();
  template.innerHTML = html;
  return template.content.firstElementChild;
};

export const buildTargetPath = (element: HTMLElement): number[] => {
  let path = [];
  while (element) {
    path.push(element);
    element = element.parentElement;
  }
  const cut = window.top === window.self ? -1 : -3;
  const indexPath = [];
  path = path.slice(0, cut);
  if (path.length === 1) {
    return [];
  }
  for (let i = path.length - 2; i >= 0; i--) {
    const child = path[i];
    const parent = path[i + 1];
    const index = Array.prototype.slice.call(parent.children).findIndex(el => el === child);
    indexPath.push(index);
  }
  return indexPath;
};
