interface IPoint {
  x: number;
  y: number;
}

export class Box {
  width: number;
  height: number;
  x: number;
  x2: number;
  y: number;
  y2: number;
  zIndex: number;

  constructor() {}

  clone() {
    const c = new Box();
    c.updateDimensions(this.x, this.y, this.width, this.height, this.zIndex);
    return c;
  }

  updateDimensions(x?: number, y?: number, w?: number, h?: number, z?: number) {
    const left = x ? x : 0;
    const top = y ? y : 0;
    const width = w ? w : 0;
    const height = h ? h : 0;

    this.x = left;
    this.y = top;
    this.width = width;
    this.height = height;
    this.x2 = this.x + this.width;
    this.y2 = this.y + this.height;
    this.zIndex = z;
  }

  equalTo(box: Box): boolean {
    return this.x === box.x
      && this.y === box.y
      && this.width === box.width
      && this.height === box.height
      && this.zIndex === box.zIndex;
  }

  isPointWithin(point: IPoint): boolean {
    return this.x >= point.x
      && this.x2 <= point.x
      && this.y >= point.y
      && this.y2 <= point.y;
  }

  isPointWithinOffset(point, offset): boolean {
    return point.x >= this.x + offset
      && point.x <= this.x2 - offset
      && point.y >= this.y + offset
      && point.y <= this.y2 - offset;
  }

  distanceToPoint(point: IPoint) {
    const centerX = this.x + this.width / 2;
    const centerY = this.y + this.height / 2;
    const dx = Math.max(Math.abs(point.x - centerX) - this.width / 2, 0);
    const dy = Math.max(Math.abs(point.y - centerY) - this.height / 2, 0);
    return dx * dx + dy * dy;
  }

  onTheSameRowWith(box: Box) {
    return this.y < box.y2 && this.y2 > box.y;
  }

  onTheSameColumnWith(box: Box) {
    return this.x < box.x2 && this.x2 > box.x;
  }
}
