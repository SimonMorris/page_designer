import { Component } from './Component';

export const canParentTakeChildren = (parent: Component, children: Component | Component[]): boolean => {
  if (!parent || !children) {
    return false;
  }
  if (!Array.isArray(children)) {
    children = [children];
  }
  if (!parent.canTakeChildren(children)) {
    return false;
  }
  for (const child of children) {
    if (!parent.canTakeChild(child) || !child.canBeDroppedId(parent)) {
      return false;
    }
  }
  return true;
};
