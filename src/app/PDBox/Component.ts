import { Box } from './Box';
import { canParentTakeChildren } from './utils';

export class Component extends Box {
  id = '';
  name = '';
  altName = '';
  element: Node;
  parent: Component | null;
  label = '';
  attributes = {};
  inline = false;

  private readonly _flags = {
    canBeMoved: false,
    canBeDuplicated: false,
    canBeEdited: false,
    canBeDeleted: false,
  };

  create() {
    // @ts-ignore
    const instance = new this.constructor();
    instance.initialize();
    return instance;
  }

  initialize(): void {}

  fixate(): void {
    this._flags.canBeMoved = false;
    this._flags.canBeDuplicated = false;
    this._flags.canBeDeleted = false;
  }

  freeze(): void {
    this.fixate();
    this._flags.canBeEdited = false;
  }

  setId(id = '') {
    this.id = id;
  }

  getId(): string {
    return this.id;
  }

  setLabel(label = '') {
    this.label = label.trim().slice(0, 50);
  }

  getLabel(): string {
    if (this.label) { return this.label; }
    else if (this.getId()) { return '#' + this.getId(); }
    else { return ''; }
  }

  hasLabel() {
    return this.getLabel().length > 0;
  }

  getName(): string {
    return this.constructor.name;
  }

  getFullName(): string {
    return this.getName() + (this.id ? '#' + this.id : '');
  }

  getDragLabel(): string {
    if (this.hasLabel()) {
      return this.getName() + ' ' + this.getLabel();
    }
    return this.getName();
  }

  get domNode() {
    return this.element;
  }

  initializeAttributes(attributes) {
    Object.keys(attributes).forEach(key => {
      this.setAttribute('.', key, attributes[key]);
    });
  }

  setAttribute(path, name, value) {
    this.attributes[path][name] = value;
  }

  isChildOf(c: Component): boolean {
    let parent = this.parent;
    while (parent) {
      if (parent === c) {
        return true;
      }
      parent = parent.parent;
    }
    return false;
  }

  hasChild(): boolean {
    return false;
  }

  canTakeChild(c: Component): boolean {
    return false;
  }

  canTakeChildren(c: Component[]): boolean {
    return false;
  }

  canBeDroppedId(c: Component): boolean {
    return c !== this && !c.isChildOf(this);
  }

  canBeInsertedIn(parent: Component): boolean {
    // @ts-ignore
    return canParentTakeChildren(parent, new this.constructor());
  }
}
