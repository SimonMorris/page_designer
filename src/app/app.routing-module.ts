import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkspaceComponent } from './ui-components/workspace/workspace.component';
import { IFrameGuard } from './services/i-frame.guard';
import { ViewerFrameComponent } from './ui-components/viewer/viewer-frame/viewer-frame.component';

const routes: Routes = [
  {
    path: '',
    component: WorkspaceComponent,
  },
  {
    path: '__viewer',
    component: ViewerFrameComponent,
    canActivate: [IFrameGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
