export enum EActionType {
  SYNC = '[Iframe] Sync',
  ZOOM_IN = '[Viewer State] Zoom In',
  ZOOM_OUT = '[Viewer State] Zoom Out',
  ROTATE = '[Viewer State] Rotate',
  DEVICE_CHANGE = '[Viewer State] Device Change',
}

export enum EEventTarget {
  HOST,
  FRAME
}

export enum EOrientation {
  LANDSCAPE,
  PORTRAIT
}

export enum ETemplateGroup {
  COMPONENT = 'component',
  MEDIA = 'media',
  TEXT = 'text',
  PAGE_STRUCTURE = 'page structure',
  LAYOUT = 'layout',
  CONTENT = 'content',
  TABLE = 'table'
}
