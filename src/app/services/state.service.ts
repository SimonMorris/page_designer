import { EventEmitter, Injectable } from '@angular/core';
import { ICanvasScaleObj, IPageDesignerState } from '../interfaces';
import { ScaleOptions } from '../registry/scale_options';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  public documentUpdated: EventEmitter<any> = new EventEmitter<any>();
  public elementSelectedOutsideCanvas: EventEmitter<any> = new EventEmitter<any>();
  public highlightedElementChanged: EventEmitter<any> = new EventEmitter<any>();

  public scaleOptions: ICanvasScaleObj = ScaleOptions;
  public zoomOptions: number[] = [0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2];
  public state: IPageDesignerState = {
    draggingTemplate: undefined,
    panels: {
      bottomPanelExpanded: false,
      leftPanelExpanded: false,
      rightPanelExpanded: false
    },
    highlightedElement: undefined,
    highlightedElementXPath: undefined,
    iframeDOMRect: undefined,
    mouse: { x: 0, y: 0 },
    zoomLevel: 3,
    scale: this.scaleOptions['desktop-hd'],
    rotate: false
  };
  public htmlTemplate: string;
  public parsedDocument: Document;

  constructor() { }

  public setHtmlTemplate(html: string) {
    this.htmlTemplate = html;
    this.parsedDocument = new DOMParser().parseFromString(this.htmlTemplate, 'text/html');
    this.documentUpdated.emit();
  }

  public getXPathForElement(element) {
    const idx = (sib, name?) => sib
      ? idx(sib.previousElementSibling, name || sib.localName) + (sib.localName === name)
      : 1;
    const segs = elm => !elm || elm.nodeType !== 1
      ? ['']
      : elm.id && document.getElementById(elm.id) === elm
        ? [`id("${ elm.id }")`]
        : [...segs(elm.parentNode), `${ elm.localName.toLowerCase() }[${ idx(elm) }]`];
    return segs(element).join('/');
  }

  public getParsedDocumentHTML(): string {
    return this.parsedDocument.body.innerHTML;
  }

  public getElementFromModel(selector: string): Element | undefined {

    if (!selector) {
      return undefined;
    }

    const d = this.parsedDocument;
    const nodeArray = d.evaluate(selector, d.body, null, XPathResult.ANY_TYPE, null);
    const node = nodeArray.iterateNext();

    if (node.nodeType === 1) {
      return node as Element;
    }

    return undefined;

  }

  public setHighlightedElement(el: Element): void {
    this.state.highlightedElement = el;
    this.highlightedElementChanged.emit();
  }

  public setHighlightedElementXPath(xpath: string) {
    this.state.highlightedElementXPath = xpath;
    this.state.highlightedElement = this.getElementFromModel(xpath);
    this.highlightedElementChanged.emit();
  }

}
