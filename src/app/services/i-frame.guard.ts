import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class IFrameGuard implements CanActivate {
  constructor(private router: Router) {}

  public canActivate(): boolean {
    try {
      if (window.self !== window.top) throw new Error();
      this.router.navigateByUrl('/');
      return false;
    } catch (e) {
      return true;
    }
  }
}
