import { Component, Input, OnInit } from '@angular/core';
import { StateService } from '../../../../services/state.service';

@Component({
  selector: 'app-html-structure-line',
  templateUrl: './htmlstructure-line.component.html',
  styleUrls: ['./htmlstructure-line.component.scss']
})
export class HtmlstructureLineComponent implements OnInit {

  @Input() element: any;
  @Input() level: number;
  public nextLevel: number;
  public elementType: string;
  public numberOfElementChildren = 0;
  public xpath = '';

  constructor(public stateSvc: StateService) { }

  ngOnInit(): void {
    this.element.expanded = false;
    this.nextLevel = this.level + 1;
    this.elementType = this.element.constructor.name;
    for (const child of this.element.children) {
      this.numberOfElementChildren = this.numberOfElementChildren + 1;
    }
    this.xpath = this.stateSvc.getXPathForElement(this.element);
  }

  public getWidth(): number {
    return this.level * 10;
  }

  public select(element) {
    this.stateSvc.setHighlightedElementXPath(this.xpath);
    this.stateSvc.elementSelectedOutsideCanvas.emit();
  }

  public checkHighlight() {
    return this.xpath === this.stateSvc.state.highlightedElementXPath;
  }

}
