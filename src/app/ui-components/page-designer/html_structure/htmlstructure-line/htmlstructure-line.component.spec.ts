import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlstructureLineComponent } from './html-blocks-element.component';

describe('HtmlBlocksElementComponent', () => {
  let component: HtmlstructureLineComponent;
  let fixture: ComponentFixture<HtmlstructureLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlstructureLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlstructureLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
