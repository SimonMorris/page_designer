import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlstructureComponent } from './html-blocks.component';

describe('HtmlBlocksComponent', () => {
  let component: HtmlstructureComponent;
  let fixture: ComponentFixture<HtmlstructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlstructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlstructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
