import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { StateService } from '../../../../services/state.service';


@Component({
  selector: 'app-html-structure',
  templateUrl: './htmlstructure.component.html',
  styleUrls: ['./htmlstructure.component.scss']
})
export class HtmlstructureComponent implements OnInit {

  constructor(
    public stateSvc: StateService
  ) { }

  ngOnInit(): void {}

}
