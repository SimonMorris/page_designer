import { Component, OnInit, Input } from '@angular/core';
import { StateService } from '../../../../services/state.service';

@Component({
  selector: 'app-cssviewer',
  templateUrl: './cssviewer.component.html',
  styleUrls: ['./cssviewer.component.scss']
})
export class CssviewerComponent implements OnInit {

  @Input() window: Window;
  public style: CSSStyleDeclaration;

  constructor(public stateSvc: StateService) { }

  ngOnInit(): void {
    this.stateSvc.highlightedElementChanged.subscribe(() => {
      this.recalculateStyle();
    });
    this.recalculateStyle();
  }

  public recalculateStyle() {
    const selector = this.stateSvc.state.highlightedElementXPath;
    const d = this.window.document;
    const nodeArray = d.evaluate(selector, d.body, null, XPathResult.ANY_TYPE, null);
    const node = nodeArray.iterateNext();
    const styles = this.getMatchedCSSRules(node);
    console.log(styles);
  }

  public getMatchedCSSRules(el, css = el.ownerDocument.styleSheets) {

    const cssRules = [];
    for (const sheet of css) {
      try {
        const rules = sheet.cssRules;
        for (const rule of rules) {
          if (el.matches(rule.selectorText)) {
            console.log(`MATCH: ${rule.selectorText}`);
            cssRules.push(sheet);
          } else {
            console.log(`NO MATCH: ${rule.selectorText}`);
          }
        }

      } catch (err) {
        console.log(`ERROR READING SHEET: ${sheet.href}`);
      }
    }
    return cssRules;

    // return [].concat(...[...css].map(s => [...s.cssRules || []]))
    //   .filter(r => el.matches(r.selectorText));
  }

}
