import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CssviewerComponent } from './cssviewer.component';

describe('CssviewerComponent', () => {
  let component: CssviewerComponent;
  let fixture: ComponentFixture<CssviewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CssviewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CssviewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
