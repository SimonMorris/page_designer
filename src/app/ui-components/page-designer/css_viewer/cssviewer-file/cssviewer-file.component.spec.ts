import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CssviewerFileComponent } from './cssviewer-file.component';

describe('CssviewerFileComponent', () => {
  let component: CssviewerFileComponent;
  let fixture: ComponentFixture<CssviewerFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CssviewerFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CssviewerFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
