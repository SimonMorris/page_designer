import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import { fromEvent, fromEventPattern, interval, Observable, Subject } from 'rxjs';
import { debounce } from 'rxjs/operators';
import { CdkDragRelease, CdkDragStart } from '@angular/cdk/drag-drop';

import { ICanvasScale, IPageDesignerTemplate, IPageDesignerTemplateCategory } from '../../interfaces';

import { StateService } from '../../services/state.service';

import { TextTemplates } from '../../registry/text';
import { MediaTemplates } from '../../registry/media';
import { PageStructureTemplates } from '../../registry/page_structure';
import { ComponentTemplates } from '../../registry/components';

declare var $: any;

@Component({
  selector: 'app-page-designer',
  templateUrl: './page-designer.component.html',
  styleUrls: ['./page-designer.component.scss']
})
export class PageDesignerComponent implements OnInit, AfterViewInit {

  public objectkeys = Object.keys;

  public templates: IPageDesignerTemplateCategory[] = [
    {title: 'Components', templates: ComponentTemplates},
    {title: 'Page Structure', templates: PageStructureTemplates},
    {title: 'Text', templates: TextTemplates},
    {title: 'Media', templates: MediaTemplates}];

  @ViewChild('highlightbox') highlightBox;
  @ViewChild('canvasiframe') canvasIFrame;
  @ViewChild('iframeoverlay') iframeOverlay;
  @ViewChild('dropPlaceholder') dropPlaceholder;
  @ViewChild('focusbar') focusBar;

  private documentScrollObservable: Observable<Event> = fromEvent(document, 'scroll').pipe(debounce(() => interval(200)));
  private iframeMouseClickObservable: Observable<any>;
  private mouseMoveObservable: Observable<Event> = fromEvent(document, 'mousemove').pipe(debounce(() => interval(150)));
  private windowResizeObservable: Observable<Event> = fromEvent(window, 'resize').pipe(debounce(() => interval(250)));
  private stateChangeObservable: Subject<void> = new Subject<void>();

  public htmlDocument;

  constructor(
    private renderer: Renderer2,
    public stateSvc: StateService,
    private changedetector: ChangeDetectorRef
  ) {
    let htmlDocument = '<html>';
    htmlDocument    += ' <head>';
    htmlDocument    += '  <meta charset="utf-8">';
    htmlDocument    += '  <title>ZontallyPortal</title>';
    htmlDocument    += '  <base href="/static/dist/zontally-worker">';
    htmlDocument    += '  <meta name="viewport" content="width=device-width, initial-scale=1">';
    htmlDocument    += '  <link rel="icon" type="image/x-icon" href="favicon.ico">';
    htmlDocument    += '  <link rel="stylesheet" href="http://demo.zontally.com/static/dist/zontally-portal/styles.css">';
    htmlDocument    += '  <script src="http://demo.zontally.com/static/dist/zontally-portal/scripts.js" defer></script>';
    htmlDocument    += '  <script src="/assets/page_designer.js" defer></script>';
    htmlDocument    += '  <link rel="stylesheet" href="/assets/page_designer.css">';
    htmlDocument    += ' </head>';
    htmlDocument    += ' <body class="h-100">Loading</body>';
    htmlDocument    += '</html>';
    this.htmlDocument = htmlDocument;
  }

  public ngOnInit(): void {
    for (const t of this.templates) {
      t.expanded = false;
    }
  }

  public ngAfterViewInit(): void {

    this.iframeMouseClickObservable = fromEventPattern(
      (handler) => {
        return $('body').on('iframeclick', handler);
      }
    );

    this.stateSvc.state.iframeDOMRect = this.canvasIFrame.nativeElement.getBoundingClientRect();
    this.stateSvc.setHtmlTemplate('<h1 id="testheader">Hello World</h1><h2 id="h2headertest">Foo and Bar</h2><h3 id="h3headertest">Ham and Spam</h3>');


    this.subscribeToMouseClick();
    this.subscribeToMouseMove();
    this.subscribeToDocumentScroll();
    this.subscribeToWindowResize();
    this.subscribeToStateChanges();
    this.subscribeToElementSelectsOutsideCanvas();
  }

  public commitChanges() {
    console.log('Commiting changes to canvas');
    this.canvasIFrame.nativeElement.contentDocument.body.innerHTML = this.stateSvc.getParsedDocumentHTML();
    this.stateSvc.documentUpdated.emit();
  }

  public panelExpanded(side: string): boolean {
    if (side === 'left') {
      return this.stateSvc.state.panels.leftPanelExpanded;
    } else if (side === 'right') {
      return this.stateSvc.state.panels.rightPanelExpanded;
    } else if (side === 'bottom') {
      return this.stateSvc.state.panels.bottomPanelExpanded;
    }
    return false;
  }

  public togglePanel(side: string): void {
    if (side === 'left') {
      this.stateSvc.state.panels.leftPanelExpanded = !this.stateSvc.state.panels.leftPanelExpanded;
    } else if (side === 'right') {
      this.stateSvc.state.panels.rightPanelExpanded = !this.stateSvc.state.panels.rightPanelExpanded;
    } else if (side === 'bottom') {
      this.stateSvc.state.panels.bottomPanelExpanded = !this.stateSvc.state.panels.bottomPanelExpanded;
    } else if (side === 'collapse-all') {
      this.stateSvc.state.panels.leftPanelExpanded = false;
      this.stateSvc.state.panels.rightPanelExpanded = false;
      this.stateSvc.state.panels.bottomPanelExpanded = false;
    }
    this.stateSvc.state.iframeDOMRect = this.canvasIFrame.nativeElement.getBoundingClientRect();
    this.stateChangeObservable.next();
  }

  private mouseOverFocusBar(): boolean {
    const rect: DOMRect = this.focusBar.nativeElement.getBoundingClientRect();
    return this.mouseInRect(rect);
  }

  private mouseInRect(rect: DOMRect): boolean {
    const left = rect.left;
    const right = rect.right;
    const top = rect.top;
    const bottom = rect.bottom;

    const isInXAxis = this.stateSvc.state.mouse.x > left && this.stateSvc.state.mouse.x < right;
    const isInYAxis = this.stateSvc.state.mouse.y > top && this.stateSvc.state.mouse.y < bottom;

    return isInXAxis && isInYAxis;
  }

  private subscribeToElementSelectsOutsideCanvas() {
    this.stateSvc.elementSelectedOutsideCanvas.subscribe(() => {
      const d = this.canvasIFrame.nativeElement.contentWindow.document;
      const elArray = d.evaluate(this.stateSvc.state.highlightedElementXPath, d.body, null, XPathResult.ANY_TYPE, null);
      this.stateSvc.state.highlightedElement = elArray.iterateNext();
      this.setHighlightBox();
      this.setFocusBar();
    });
  }

  private subscribeToWindowResize() {
    this.windowResizeObservable.subscribe((event: Event) => {
      this.stateSvc.state.iframeDOMRect = this.canvasIFrame.nativeElement.getBoundingClientRect();
      this.stateChangeObservable.next();
    });
  }

  private subscribeToDocumentScroll() {
    this.documentScrollObservable.subscribe((event: Event) => {
      this.stateSvc.state.iframeDOMRect = this.canvasIFrame.nativeElement.getBoundingClientRect();
      this.stateChangeObservable.next();
    });
  }

  private subscribeToMouseClick() {

    this.iframeMouseClickObservable.subscribe((ev) => {

      const selector = ev[1].selector;
      const d = this.stateSvc.parsedDocument;
      const elArray = d.evaluate(selector, d.body, null, XPathResult.ANY_TYPE, null);
      const el = elArray.iterateNext();

      const xpath = this.stateSvc.getXPathForElement(el);
      this.stateSvc.state.highlightedElement = this.stateSvc.getElementFromModel(xpath);
      this.stateSvc.state.highlightedElementXPath = xpath;

      this.changedetector.detectChanges();
      this.setHighlightBox();
      this.setFocusBar();
      this.stateChangeObservable.next();
    });

  }

  private subscribeToMouseMove() {
    this.mouseMoveObservable.subscribe((event: MouseEvent) => {

      // If not dragging a template ignore the mousemove
      const isDraggingTemplate = this.stateSvc.state.hasOwnProperty('draggingTemplate') && this.stateSvc.state.draggingTemplate;
      if (!isDraggingTemplate) {
        return;
      }

      this.stateSvc.state.mouse.x = event.pageX;
      this.stateSvc.state.mouse.y = event.pageY;

      // If not over the IFrame overlay ignore the mousemove
      const overlayRect: DOMRect = this.iframeOverlay.nativeElement.getBoundingClientRect();
      if (!this.mouseInRect(overlayRect)) {
        return;
      }

      const iframeDoc: Document = this.canvasIFrame.nativeElement.contentWindow.document;

      const pageX = event.pageX;
      const pageY = event.pageY;
      console.log(`MOUSE STOPPED at ${pageX}/${pageY}`);

      const iFrameX = pageX - this.stateSvc.state.iframeDOMRect.left;
      const iFrameY = pageY - this.stateSvc.state.iframeDOMRect.top;
      console.log(`Inspecting iFRAME co-ordinates ${iFrameX}/${iFrameY}`);

      this.stateSvc.state.highlightedElement = iframeDoc.elementFromPoint(iFrameX, iFrameY);
      console.log(`Highlighted Element: ${this.stateSvc.getXPathForElement(this.stateSvc.state.highlightedElement)}`);
      this.setDropPlaceholder();
    });
  }


  private setHighlightBox(): void {

    const mouseOverFocusBar = this.mouseOverFocusBar();
    const isDraggingTemplate = this.stateSvc.state.hasOwnProperty('draggingTemplate') && this.stateSvc.state.draggingTemplate;

    if (mouseOverFocusBar) {
      return; // If we are over the focus bar don't move it
    }

    if (!mouseOverFocusBar && !isDraggingTemplate) {
      const rect = this.stateSvc.state.highlightedElement.getBoundingClientRect();
      console.log('Setting highlighter box');
      this.renderer.setStyle(this.highlightBox.nativeElement, 'border-color', 'red');
      this.renderer.setStyle(this.highlightBox.nativeElement, 'border-style', 'solid');
      this.renderer.setStyle(this.highlightBox.nativeElement, 'border-width', '1px');
      this.renderer.setStyle(this.highlightBox.nativeElement, 'top', `${rect.top + 38}px`);
      this.renderer.setStyle(this.highlightBox.nativeElement, 'left', `${rect.left}px`);
      this.renderer.setStyle(this.highlightBox.nativeElement, 'height', `${rect.height}px`);
      this.renderer.setStyle(this.highlightBox.nativeElement, 'width', `${rect.width}px`);
      this.renderer.setStyle(this.highlightBox.nativeElement, 'position', 'absolute');
      this.renderer.setStyle(this.highlightBox.nativeElement, 'display', 'block');
    } else {
      this.renderer.setStyle(this.highlightBox.nativeElement, 'display', 'none');
    }

  }

  private setFocusBar() {

    const mouseOverFocusBar = this.mouseOverFocusBar();
    const isDraggingTemplate = this.stateSvc.state.hasOwnProperty('draggingTemplate') && this.stateSvc.state.draggingTemplate;

    if (mouseOverFocusBar) {
      return; // If we are over the focus bar don't move it
    }

    if (!mouseOverFocusBar && !isDraggingTemplate) {
      const rect = this.stateSvc.state.highlightedElement.getBoundingClientRect();
      console.log('Setting focus bar');
      this.renderer.setStyle(this.focusBar.nativeElement, 'top', `${rect.top + 19}px`);
      this.renderer.setStyle(this.focusBar.nativeElement, 'left', `${rect.left}px`);
      this.renderer.setStyle(this.focusBar.nativeElement, 'display', 'block');
    } else {
      this.renderer.setStyle(this.focusBar.nativeElement, 'display', 'none');
    }
  }

  private setDropPlaceholder() {
    if (this.stateSvc.state.draggingTemplate) {
      const rect = this.stateSvc.state.highlightedElement.getBoundingClientRect();
      this.renderer.setStyle(this.dropPlaceholder.nativeElement, 'border-bottom-color', 'green');
      this.renderer.setStyle(this.dropPlaceholder.nativeElement, 'border-bottom-style', 'solid');
      this.renderer.setStyle(this.dropPlaceholder.nativeElement, 'border-bottom-width', '1px');
      this.renderer.setStyle(this.dropPlaceholder.nativeElement, 'top', `${rect.top + rect.height + 38}px`);
      this.renderer.setStyle(this.dropPlaceholder.nativeElement, 'left', `${rect.left}px`);
      this.renderer.setStyle(this.dropPlaceholder.nativeElement, 'height', '1px');
      this.renderer.setStyle(this.dropPlaceholder.nativeElement, 'width', `${rect.width}px`);
      this.renderer.setStyle(this.dropPlaceholder.nativeElement, 'position', 'absolute');
      this.renderer.setStyle(this.dropPlaceholder.nativeElement, 'display', 'block');
    } else {
      this.renderer.setStyle(this.dropPlaceholder.nativeElement, 'display', 'none');

    }
  }

  public templateDragStart(event: CdkDragStart) {
    this.stateSvc.state.draggingTemplate = event.source.data;
    this.sizeIFrameOverlay();
  }

  public sizeIFrameOverlay() {
    if (this.stateSvc.state.draggingTemplate) {
      const rect = this.canvasIFrame.nativeElement.getBoundingClientRect();
      this.renderer.setStyle(this.iframeOverlay.nativeElement, 'top', `35px`);
      this.renderer.setStyle(this.iframeOverlay.nativeElement, 'left', `0px`);
      this.renderer.setStyle(this.iframeOverlay.nativeElement, 'height', `${rect.height}px`);
      this.renderer.setStyle(this.iframeOverlay.nativeElement, 'width', `${rect.width}px`);
      this.renderer.setStyle(this.iframeOverlay.nativeElement, 'position', 'absolute');
      this.renderer.setStyle(this.iframeOverlay.nativeElement, 'display', 'block');
    } else {
      this.renderer.setStyle(this.iframeOverlay.nativeElement, 'display', 'none');
      this.renderer.setStyle(this.iframeOverlay.nativeElement, 'top', 0);
      this.renderer.setStyle(this.iframeOverlay.nativeElement, 'left', 0);
      this.renderer.setStyle(this.iframeOverlay.nativeElement, 'height', '1px');
      this.renderer.setStyle(this.iframeOverlay.nativeElement, 'width', '1px');
    }
  }

  public templateDragRelease(event: CdkDragRelease) {

    console.log('Apply template');
    const template: IPageDesignerTemplate = event.source.data;
    const iframeDocument = this.canvasIFrame.nativeElement.contentWindow.document;
    $(this.stateSvc.state.highlightedElement).after(template.template);
    this.stateSvc.state.draggingTemplate = undefined;
    this.setDropPlaceholder();
    this.sizeIFrameOverlay();
    const body = iframeDocument.getElementsByTagName('body');
    this.stateSvc.setHtmlTemplate($(body).html());
    const ev = new Event('documented_updated');
    this.canvasIFrame.nativeElement.contentWindow.document.body.dispatchEvent(ev);
  }

  private subscribeToStateChanges() {
    this.stateChangeObservable.subscribe(() => {
      this.setFocusBar();
      // this.setHighlightBox();
      this.setDropPlaceholder();
    });
  }

  public focusBarDelete() {
    $(this.stateSvc.state.highlightedElement).remove();
    const iframeDocument = this.canvasIFrame.nativeElement.contentWindow.document;
    const body = iframeDocument.getElementsByTagName('body');
    this.stateSvc.setHtmlTemplate($(body).html());
  }

  public changeScale(scale: ICanvasScale | null, rotate: boolean) {
    this.stateSvc.state.scale = scale;
    this.stateSvc.state.rotate = rotate;
  }

  public zoom(zoom: string) {
    if (zoom === 'out') {
      if (this.stateSvc.state.zoomLevel > 1) {
        this.stateSvc.state.zoomLevel = this.stateSvc.state.zoomLevel - 1;
      }
    } else {
      if (this.stateSvc.state.zoomLevel < this.stateSvc.zoomOptions.length) {
        this.stateSvc.state.zoomLevel = this.stateSvc.state.zoomLevel + 1;
      }
    }
    this.renderer.setStyle(this.canvasIFrame.nativeElement, 'transform', `scale(${this.stateSvc.zoomOptions[this.stateSvc.state.zoomLevel]})`);
    console.log(`Scale to ${this.stateSvc.state.zoomLevel}`);
  }

  public rotateCanvas() {
    const w = this.stateSvc.state.scale.width;
    const h = this.stateSvc.state.scale.height;
    this.stateSvc.state.scale.height = w;
    this.stateSvc.state.scale.width = h;
  }

}
