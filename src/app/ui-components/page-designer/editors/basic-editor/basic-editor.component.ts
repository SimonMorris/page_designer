import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-basic-editor',
  templateUrl: './basic-editor.component.html',
  styleUrls: ['./basic-editor.component.scss']
})
export class BasicEditorComponent implements OnInit {

  @Input() public element: Element;
  @Input() public xpath: string;
  @Output() public updated: EventEmitter<void> = new EventEmitter<void>();

  private subject: Subject<void> = new Subject();

  public id: string;

  constructor() { }

  ngOnInit(): void {
    this.id = this.element.id;
    console.log(`id=${this.id}`);
    this.subject.pipe(debounceTime(500)).subscribe(searchTextValue => {
      this.updated.emit();
    });
  }

  onKeyUp(){
    this.subject.next();
  }

}
