import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HtmlviewerLineComponent } from './htmlviewer-line.component';

describe('HtmlviewerLineComponent', () => {
  let component: HtmlviewerLineComponent;
  let fixture: ComponentFixture<HtmlviewerLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HtmlviewerLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlviewerLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
