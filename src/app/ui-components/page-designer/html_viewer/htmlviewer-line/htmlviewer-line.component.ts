import { Component, Input, OnInit } from '@angular/core';
import { StateService } from '../../../../services/state.service';

@Component({
  selector: 'app-htmlviewer-line',
  templateUrl: './htmlviewer-line.component.html',
  styleUrls: ['./htmlviewer-line.component.scss']
})
export class HtmlviewerLineComponent implements OnInit {
  @Input() element: any;
  @Input() level: number;
  public nextLevel: number;
  public elementType: string;
  public numberOfElementChildren = 0;
  public xpath: string;

  constructor(private stateSvc: StateService) { }

  ngOnInit(): void {
    this.element.expanded = true;
    this.nextLevel = this.level + 1;
    this.elementType = this.element.constructor.name;
    for (const child of this.element.children) {
      this.numberOfElementChildren = this.numberOfElementChildren + 1;
    }
    this.xpath = this.stateSvc.getXPathForElement(this.element);
  }

  public getWidth(): number {
    return this.level * 10;
  }

  public select(element) {
    this.stateSvc.state.highlightedElementXPath = this.xpath;
    this.stateSvc.elementSelectedOutsideCanvas.emit();
  }

  public checkHighlight() {
    return this.xpath === this.stateSvc.state.highlightedElementXPath;
  }

}
