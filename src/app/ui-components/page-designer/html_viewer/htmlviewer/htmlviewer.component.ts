import { Component, Input, OnInit } from '@angular/core';
import { StateService } from '../../../../services/state.service';


@Component({
  selector: 'app-htmlviewer',
  templateUrl: './htmlviewer.component.html',
  styleUrls: ['./htmlviewer.component.scss']
})
export class HtmlviewerComponent implements OnInit {

  constructor(
    public stateSvc: StateService
  ) { }

  ngOnInit(): void {}

}
