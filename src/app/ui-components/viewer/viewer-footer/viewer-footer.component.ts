import { Component, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-viewer-footer',
  templateUrl: './viewer-footer.component.html',
  styleUrls: ['./viewer-footer.component.scss']
})
export class ViewerFooterComponent {
  @Input() isExpanded: BehaviorSubject<boolean>;

  togglePanel() {
    this.isExpanded.next(!this.isExpanded.value);
  }
}
