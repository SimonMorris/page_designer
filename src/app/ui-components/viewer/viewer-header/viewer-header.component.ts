import { Component, ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { ICanvasScale, State } from '../../../interfaces';
import { device, rotate, zoomIn, zoomOut } from '../../../store/actions';
import { viewerHeight, viewerWidth, zoomLevel } from '../../../store/selectors';
import { ScaleOptions } from '../../../registry/scale_options';
import { EOrientation } from '../../../enums';

@Component({
  selector: 'app-viewer-header',
  templateUrl: './viewer-header.component.html',
  styleUrls: ['./viewer-header.component.scss']
})
export class ViewerHeaderComponent {
  readonly SCALE_OPTIONS = ScaleOptions;
  readonly LANDSCAPE = EOrientation.LANDSCAPE;

  zoomLevel$ = this._store.select(zoomLevel);
  width$ = this._store.select(viewerWidth);
  height$ = this._store.select(viewerHeight);

  constructor(public _store: Store<State>,
              private _host: ElementRef<HTMLElement>) {}

  zoomIn() {
    this._store.dispatch(zoomIn());
  }

  zoomOut() {
    this._store.dispatch(zoomOut());
  }

  expandCanvas() {
    const event = new CustomEvent('expandCanvas', { bubbles: true });
    this._host.nativeElement.dispatchEvent(event);
  }

  rotateCanvas() {
    this._store.dispatch(rotate());
  }

  pickScale(option: ICanvasScale, orientation?: EOrientation) {
    if (typeof orientation !== 'number') {
      orientation = option.width > option.height ? EOrientation.LANDSCAPE : EOrientation.PORTRAIT;
    }
    this._store.dispatch(device({ scale: option, orientation }));
  }
}
