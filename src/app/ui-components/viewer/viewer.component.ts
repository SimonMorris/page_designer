import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../interfaces';
import {
  scaledHeight,
  scaledWidth,
  viewerHeight,
  viewerWidth,
  zoomLevel
} from '../../store/selectors';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Subject } from 'rxjs';
import { dispatchCustomEvent } from '../../utils';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnDestroy {
  @ViewChild('canvas') private _canvasRef: ElementRef<HTMLElement>;
  private readonly _destroy = new Subject();

  width$ = this._store.select(viewerWidth);
  height$ = this._store.select(viewerHeight);
  scaledWidth$ = this._store.select(scaledWidth);
  scaledHeight$ = this._store.select(scaledHeight);
  offsetTop$ = this.scaledHeight$.pipe(map(h => h / -2));
  offsetLeft$ = this.scaledWidth$.pipe(map(w => w / -2));
  scale$ = this._store.select(zoomLevel);
  isFooterExpanded$ = new BehaviorSubject(false);

  constructor(private _store: Store<State>) {}

  ngOnDestroy() {
    this._destroy.next();
    this._destroy.complete();
  }

  hideFooter() {
    this.isFooterExpanded$.next(false);
  }

  clearActiveTargetData(event: MouseEvent) {
    if (event.target === this._canvasRef.nativeElement) {
      dispatchCustomEvent('iframe:deselect', event);
    }
  }
}
