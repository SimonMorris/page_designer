import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { IDragData, IDropData, State } from '../../../interfaces';
import { Store } from '@ngrx/store';
import { zoomLevel } from '../../../store/selectors';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { debounceTime, map, takeUntil, withLatestFrom } from 'rxjs/operators';
import { getBoxParams } from '../../../utils';



@Component({
  selector: 'app-viewer-overlay',
  templateUrl: './viewer-overlay.component.html',
  styleUrls: ['./viewer-overlay.component.scss']
})
export class ViewerOverlayComponent implements OnInit, OnDestroy {
  private readonly _destroy = new Subject();
  private _hoverElement = new BehaviorSubject<HTMLElement>(null);
  private _activeElement = new BehaviorSubject<HTMLElement>(null);
  dropData: IDropData;

  hoverData$ = this._hoverElement.pipe(
    withLatestFrom(this._store.select(zoomLevel)),
    map(([el, zoom]) => getBoxParams(el, zoom)));
  activeData$ = this._activeElement.pipe(
    withLatestFrom(this._store.select(zoomLevel)),
    map(([el, zoom]) => getBoxParams(el, zoom)));

  hasParent$ = this._activeElement.pipe(map(el => !el || el.id !== 'body'));
  isNotSameTarget$ = combineLatest([this._hoverElement, this._activeElement])
    .pipe(map(([h, a]) => h !== a));

  constructor(private _store: Store<State>) {}

  ngOnInit() {
    this._store.pipe(takeUntil(this._destroy), debounceTime(10))
      .subscribe(() => this._activeElement.next(this._activeElement.value));
  }

  ngOnDestroy() {
    this._destroy.next();
    this._destroy.complete();
  }

  @HostListener('window:iframe:textupdate')
  updateActiveElementBox() {
    this._activeElement.next(this._activeElement.value);
  }

  @HostListener('window:iframe:mousemove', ['$event'])
  iframeHoverStart(event: CustomEvent<MouseEvent>) {
    this._hoverElement.next(event.detail.target as HTMLElement);
  }

  @HostListener('window:iframe:mouseleave', ['$event'])
  iframeHoverEnd(event: CustomEvent) {
    this._hoverElement.next(null);
  }

  @HostListener('window:iframe:select', ['$event'])
  iframeSelectStart(event: CustomEvent<MouseEvent>) {
    this._activeElement.next(event.detail.target as HTMLElement);
  }

  @HostListener('window:iframe:deselect', ['$event'])
  iframeSelectEnd(event: CustomEvent) {
    this._activeElement.next(null);
  }

  @HostListener('window:iframe:dragging', ['$event'])
  iframeDragging(event: CustomEvent<IDragData>) {
    this._hoverElement.next(event.detail.target);
    this.dropData = event.detail.dropData;
  }

  @HostListener('window:iframe:drop', ['$event'])
  iframeDrop(event: CustomEvent<MouseEvent>) {
    if (this.dropData) {
      this.dropData.dropFn();
      this.dropData = null;
    }
  }

  @HostListener('window:iframe:editstart')
  editStart() {
    this.edit();
  }

  @HostListener('window:dragcancel')
  dragCancel() {
    this.dropData = null;
    this._hoverElement.next(null);
  }

  selectParent() {
    if (!this._canManipulate()) {
      return;
    }
    this._activeElement.value.parentNode.dispatchEvent(new MouseEvent('click', { bubbles: true }));
  }

  copy() {
    if (!this._canManipulate()) {
      return;
    }
    const element = this._activeElement.value.cloneNode(true) as Element;
    this._activeElement.value.insertAdjacentElement('afterend', element);
  }

  move() {
    if (!this._canManipulate()) {
      return;
    }
  }

  edit() {
    if (!this._canManipulate()) {
      return;
    }
    const blurListener = () => {
      this._activeElement.value.contentEditable = 'false';
      this._activeElement.value.removeEventListener('blur', blurListener);
    };
    this._activeElement.value.addEventListener('blur', blurListener);
    this._activeElement.value.contentEditable = 'true';
  }

  remove() {
    if (!this._canManipulate()) {
      return;
    }
    const nextSibling = this._activeElement.value.nextElementSibling
      || this._activeElement.value.previousElementSibling;
    this._activeElement.value.remove();
    this._activeElement.next(nextSibling as HTMLElement);
  }

  private _canManipulate() {
    return this._activeElement.value && this._activeElement.value.id !== 'body';
  }
}
