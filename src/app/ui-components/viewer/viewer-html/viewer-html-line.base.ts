import { Input } from '@angular/core';

export class ViewerHtmlLineBase {
  @Input() disabled = false;
  @Input() element: HTMLElement;
  expanded = true;

  toggleExpanded(event: MouseEvent) {
    event.stopPropagation();
    this.expanded = !this.expanded;
  }

  select(event: MouseEvent) {
    event.stopPropagation();
    this.element.click();
  }

  highlight(event: MouseEvent) {
    event.stopPropagation();
    this.element.dispatchEvent(new CustomEvent('mouseover', { bubbles: true }));
  }

  removeHighlight(event: MouseEvent) {
    event.stopPropagation();
    this.element.dispatchEvent(new CustomEvent('mouseout', { bubbles: true }));
  }
}
