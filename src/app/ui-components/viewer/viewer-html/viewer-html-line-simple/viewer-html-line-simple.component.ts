import { Component, HostBinding, Input } from '@angular/core';
import { ViewerHtmlLineBase } from '../viewer-html-line.base';

@Component({
  selector: 'app-viewer-html-line-simple',
  templateUrl: './viewer-html-line-simple.component.html',
  styleUrls: ['./viewer-html-line-simple.component.scss']
})
export class ViewerHtmlLineSimpleComponent extends ViewerHtmlLineBase {
  @HostBinding('class.hidden') @Input() hidden = false;

  constructor() {
    super();
  }
}
