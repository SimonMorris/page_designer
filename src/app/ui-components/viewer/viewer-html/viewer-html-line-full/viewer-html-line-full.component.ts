import { Component } from '@angular/core';
import { ViewerHtmlLineBase } from '../viewer-html-line.base';

@Component({
  selector: 'app-viewer-html-line-full',
  templateUrl: './viewer-html-line-full.component.html',
  styleUrls: ['./viewer-html-line-full.component.scss']
})
export class ViewerHtmlLineFullComponent extends ViewerHtmlLineBase {
  constructor() {
    super();
  }
}
