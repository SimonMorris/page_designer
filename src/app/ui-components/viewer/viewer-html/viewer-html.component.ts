import { Component, HostListener, Input, OnDestroy } from '@angular/core';
import { buildTargetPath, dispatchCustomEvent } from '../../../utils';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EEventTarget } from '../../../enums';

@Component({
  selector: 'app-viewer-html',
  templateUrl: './viewer-html.component.html',
  styleUrls: ['./viewer-html.component.scss']
})
export class ViewerHtmlComponent implements OnDestroy {
  @Input() type: 'full' | 'simple';

  private _currentSelected: HTMLElement;
  private readonly _destroy = new Subject();
  private readonly _parser = new DOMParser();
  html: Document;

  @HostListener('window:iframe:deselect', ['$event'])
  removeSelected() {
    if (this._currentSelected) {
      delete this._currentSelected.dataset.selected;
      this._currentSelected = null;
    }
  }

  @HostListener('window:iframe:select', ['$event'])
  setSelected(event: CustomEvent<{ path: number[] }>) {
    this.removeSelected();
    let element = this.html.body;
    event.detail.path.forEach(index => {
      element = element.children[index] as HTMLElement;
    });
    this._currentSelected = element;
    element.dataset.selected = 'true';
  }

  @HostListener('window:iframe:textupdate', ['$event'])
  updateTextContent(event: CustomEvent<{ path: number[]; innerHTML: string }>) {
    let el = this.html.body;
    event.detail.path.forEach(index => el = el.children[index] as HTMLElement);
    el.innerHTML = event.detail.innerHTML;
  }

  @HostListener('window:iframe:attributeupdate', ['$event'])
  updateAttribute(event: CustomEvent<{ path: number[]; attrName: string; attrValue: string }>) {
    if (!event.detail.path.length) {
      return;
    }
    let el = this.html.body;
    event.detail.path.forEach(index => el = el.children[index] as HTMLElement);
    el.setAttribute(event.detail.attrName, event.detail.attrValue);
  }

  @HostListener('window:iframe:templateupdate', ['$event'])
  updateTemplate(event: CustomEvent<string>) {
    this._destroy.next();
    this.html = this._parser.parseFromString(event.detail, 'text/html');

    fromEvent(this.html, 'click')
      .pipe(takeUntil(this._destroy))
      .subscribe((evt: any) => {
        const path = buildTargetPath(evt.target as HTMLElement);
        dispatchCustomEvent('viewer:select', path, EEventTarget.FRAME);
      });

    fromEvent(this.html, 'mouseover')
      .pipe(takeUntil(this._destroy))
      .subscribe((evt: any) => {
        const path = buildTargetPath(evt.target as HTMLElement);
        dispatchCustomEvent('viewer:mouseover', path, EEventTarget.FRAME);
      });

    fromEvent(this.html, 'mouseout')
      .pipe(takeUntil(this._destroy))
      .subscribe((evt: any) => {
        const path = buildTargetPath(evt.target as HTMLElement);
        dispatchCustomEvent('viewer:mouseleave', path, EEventTarget.FRAME);
      });
  }

  ngOnDestroy() {
    this._destroy.next();
    this._destroy.complete();
  }
}
