import { Component, ElementRef, HostBinding, HostListener, OnDestroy, OnInit } from '@angular/core';
import { IDragData, IDropData, IViewerState, State } from '../../../interfaces';
import { CdkDragMove } from '@angular/cdk/drag-drop';
import { Store } from '@ngrx/store';
import { sync } from '../../../store/actions';
import { buildTargetPath, clamp, dispatchCustomEvent, htmlToElement } from '../../../utils';
import { zoomLevel } from '../../../store/selectors';
import { take } from 'rxjs/operators';
import { Box } from '../../../PDBox/Box';
import { EEventTarget } from '../../../enums';

const MUTATION_CONFIG = {
  childList: true,
  subtree: true,
  characterData: true,
  attributeFilter: ['class', 'id', 'role']
};

@Component({
  selector: 'app-viewer-frame',
  template: '',
  styleUrls: ['./viewer-frame.component.scss']
})
export class ViewerFrameComponent implements OnInit, OnDestroy {
  observer = new MutationObserver((list) => {
    list.forEach(record => {
      if (record.type === 'childList') {
        dispatchCustomEvent<string>(
          'iframe:templateupdate',
          this._host.nativeElement.innerHTML,
          EEventTarget.HOST
        );
      } else if (record.type === 'characterData') {
        dispatchCustomEvent<{ path: number[], innerHTML: string }>(
          'iframe:textupdate',
          { path: buildTargetPath(record.target.parentElement), innerHTML: record.target.parentElement.innerHTML },
          EEventTarget.HOST
        );
      } else if (record.type === 'attributes') {
        dispatchCustomEvent<{ path: number[], attrName: string; attrValue: string }>(
          'iframe:attributeupdate',
          {
            path: buildTargetPath(record.target as HTMLElement),
            attrName: record.attributeName,
            attrValue: (record.target as HTMLElement).getAttribute(record.attributeName),
          },
          EEventTarget.HOST
        );
      }
    });
  });

  private readonly _clampPosition = clamp(0, Infinity);
  @HostBinding('id') private readonly _id = 'body';
  @HostListener('window:iframe:sync', ['$event'])
  syncState(event: CustomEvent<IViewerState>) {
    this._store.dispatch(sync(event.detail));
  }

  @HostListener('window:dragoverframe', ['$event'])
  dragOver(event: CustomEvent<CdkDragMove>) {
    this._store.select(zoomLevel)
      .pipe(take(1))
      .subscribe(zoom => {
        const frameRect = frameElement.getBoundingClientRect();
        const x = this._clampPosition(event.detail.pointerPosition.x - frameRect.x) / zoom;
        const y = this._clampPosition(event.detail.pointerPosition.y - frameRect.y) / zoom;
        const initiator = event.detail.source.element.nativeElement;
        const target = document.elementFromPoint(x, y) as HTMLElement;
        const pasteElement = htmlToElement(event.detail.source.data.template);
        const dropData: IDropData = {};

        if (!target.children.length) {
          dropData.dropFn = () => {
            target.appendChild(pasteElement);
            initiator.dispatchEvent(new Event('mouseup'));
          };
          dispatchCustomEvent<IDragData>('iframe:dragging', { target, dropData }, EEventTarget.HOST);
          return;
        }

        const boxes = Array.prototype.slice.call(target.children)
          .map((child, i) => {
            const box = new Box();
            const rect = child.getBoundingClientRect();
            box.updateDimensions(rect.x, rect.y, rect.width, rect.height);
            return { child, box, i };
          });

        let minDistance = Infinity;
        const anchor = boxes.reduce((el, next) => {
          if (!next.box.width || !next.box.height) {
            return el;
          }
          const d = next.box.distanceToPoint({ x, y });
          if (d < minDistance) {
            minDistance = d;
            return next;
          } else {
            return el;
          }
        }, null);

        if (x < anchor.box.x) {
          dropData.width = 1;
          dropData.height = anchor.box.height * zoom;
          dropData.left = (anchor.box.x - 8) * zoom;
          dropData.top = anchor.box.y * zoom;
          dropData.dropFn = () => {
            anchor.child.insertAdjacentElement('beforebegin', pasteElement);
            initiator.dispatchEvent(new Event('mouseup'));
          };
          dropData.className = 'left';
          if (anchor.child.previousElementSibling) {
            if (anchor.box.onTheSameRowWith(boxes[anchor.i - 1].box)) {
              dropData.className += ' right';
            }
          }
        } else if (x > (anchor.box.x + anchor.box.width)) {
          dropData.width = 1;
          dropData.height = anchor.box.height * zoom;
          dropData.left = (anchor.box.x2 + 8) * zoom;
          dropData.top = anchor.box.y * zoom;
          dropData.dropFn = () => {
            anchor.child.insertAdjacentElement('afterend', pasteElement);
            initiator.dispatchEvent(new Event('mouseup'));
          };
          dropData.className = 'right';
          if (anchor.child.nextElementSibling) {
            if (anchor.box.onTheSameRowWith(boxes[anchor.i + 1].box)) {
              dropData.className += ' left';
            }
          }
        } else if (y < anchor.box.y) {
          dropData.width = anchor.box.width * zoom;
          dropData.height = 1;
          dropData.left = anchor.box.x * zoom;
          dropData.top = (anchor.box.y - 8) * zoom;
          dropData.dropFn = () => {
            anchor.child.insertAdjacentElement('beforebegin', pasteElement);
            initiator.dispatchEvent(new Event('mouseup'));
          };
          dropData.className = 'top';
          if (anchor.child.previousElementSibling) {
            if (anchor.box.onTheSameColumnWith(boxes[anchor.i - 1].box)) {
              dropData.className += ' bottom';
            }
          }
        } else if (y > (anchor.box.y + anchor.box.height)) {
          dropData.width = anchor.box.width * zoom;
          dropData.height = 1;
          dropData.left = anchor.box.x * zoom;
          dropData.top = (anchor.box.y2 + 8)  * zoom;
          dropData.dropFn = () => {
            anchor.child.insertAdjacentElement('afterend', pasteElement);
            initiator.dispatchEvent(new Event('mouseup'));
          };
          dropData.className = 'bottom';
          if (anchor.child.nextElementSibling) {
            if (anchor.box.onTheSameColumnWith(boxes[anchor.i + 1].box)) {
              dropData.className += ' top';
            }
          }
        }

        dispatchCustomEvent<IDragData>('iframe:dragging', { target, dropData }, EEventTarget.HOST);
      });
  }

  @HostListener('mousemove', ['$event'])
  propagateHover(event: MouseEvent) {
    dispatchCustomEvent('iframe:mousemove', event, EEventTarget.HOST);
  }

  @HostListener('mouseleave', ['$event'])
  propagateLeave(event) {
    dispatchCustomEvent('iframe:mouseleave', event, EEventTarget.HOST);
  }

  @HostListener('click', ['$event'])
  propagateFocus(event: MouseEvent) {
    event.preventDefault();
    const path = buildTargetPath(event.target as HTMLElement);
    dispatchCustomEvent('iframe:select', { target: event.target, path }, EEventTarget.HOST);
  }

  @HostListener('dblclick', ['$event'])
  setContentEditable(event: MouseEvent) {
    dispatchCustomEvent('iframe:editstart', event, EEventTarget.HOST);
  }

  @HostListener('mouseup', ['$event'])
  propagateDrop(event: MouseEvent) {
    dispatchCustomEvent('iframe:drop', event, EEventTarget.HOST);
  }

  @HostListener('window:viewer:select', ['$event'])
  emitSelect(event: CustomEvent<number[]>) {
    let element = this._host.nativeElement;
    event.detail.forEach(index => {
      element = element.children[index];
    });
    element.click();
  }

  @HostListener('window:viewer:mouseover', ['$event'])
  emitMouseover(event: CustomEvent<number[]>) {
    let element = this._host.nativeElement;
    event.detail.forEach(index => {
      element = element.children[index];
    });
    element.dispatchEvent(new MouseEvent('mousemove', { bubbles: true }));
  }

  @HostListener('window:viewer:mouseleave', ['$event'])
  emitMouseout(event: CustomEvent<number[]>) {
    let element = this._host.nativeElement;
    event.detail.forEach(index => {
      element = element.children[index];
    });
    element.dispatchEvent(new MouseEvent('mouseleave', { bubbles: true }));
  }

  constructor(private _store: Store<State>, private _host: ElementRef) {}

  ngOnInit() {
    this.observer.observe(this._host.nativeElement, MUTATION_CONFIG);
    dispatchCustomEvent<string>('iframe:templateupdate', '', EEventTarget.HOST);
  }

  ngOnDestroy() {
    this.observer.disconnect();
  }
}
