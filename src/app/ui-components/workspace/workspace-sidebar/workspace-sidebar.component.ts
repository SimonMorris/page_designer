import { Component, Input } from '@angular/core';
import { State } from '../../../interfaces';
import { BehaviorSubject } from 'rxjs';
import { CdkDragMove } from '@angular/cdk/drag-drop';
import { Store } from '@ngrx/store';
import { TEMPLATE_GROUPS } from '../../../registry/templates';
import { dispatchCustomEvent } from '../../../utils';
import { EEventTarget } from '../../../enums';

@Component({
  selector: 'app-workspace-sidebar',
  templateUrl: './workspace-sidebar.component.html',
  styleUrls: ['./workspace-sidebar.component.scss']
})
export class WorkspaceSidebarComponent {
  @Input() public isExpanded: BehaviorSubject<boolean>;
  public readonly templates = TEMPLATE_GROUPS;

  constructor(private _store: Store<State>) {}

  togglePanel() {
    this.isExpanded.next(!this.isExpanded.value);
  }

  templateDragMove(event: CdkDragMove) {
    if ((event.event.target as HTMLElement).tagName === 'IFRAME') {
      dispatchCustomEvent<CdkDragMove>('dragoverframe', event, EEventTarget.FRAME);
    }
  }

  templateDragEnd() {
    dispatchCustomEvent<CdkDragMove>('dragcancel', null);
  }
}
