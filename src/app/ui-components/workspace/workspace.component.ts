import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss']
})
export class WorkspaceComponent {
  isSidebarExpanded$ = new BehaviorSubject<boolean>(true);
  isConfiguratorExpanded$ = new BehaviorSubject<boolean>(true);

  expandCanvas() {
    this.isSidebarExpanded$.next(false);
    this.isConfiguratorExpanded$.next(false);
  }
}
