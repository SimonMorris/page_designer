import { Component, HostBinding, HostListener, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-property-editor',
  templateUrl: './property-editor.component.html',
  styleUrls: ['./property-editor.component.scss']
})
export class PropertyEditorComponent {
  @HostBinding('class') private readonly _baseClass = 'md-form';
  @Input() property: string;
  @Input() label: string;
  private _activeElement = new BehaviorSubject<HTMLElement>(null);
  property$ = this._activeElement.pipe(map(el =>
    (el && el.id !== 'body') ? (el[this.property] || el.getAttribute(this.property) || '') : null));

  @HostListener('window:iframe:select', ['$event'])
  iframeSelectStart(event: CustomEvent<MouseEvent>) {
    this._activeElement.next(event.detail.target as HTMLElement);
  }

  @HostListener('window:iframe:deselect')
  iframeSelectEnd() {
    this._activeElement.next(null);
  }

  changeProperty(attrValue: string): void {
    if (this._activeElement.value) {
      this._activeElement.value.setAttribute(this.property, attrValue);
    }
  }
}
