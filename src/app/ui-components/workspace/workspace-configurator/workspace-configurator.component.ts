import { Component, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-workspace-configurator',
  templateUrl: './workspace-configurator.component.html',
  styleUrls: ['./workspace-configurator.component.scss']
})
export class WorkspaceConfiguratorComponent {
  @Input() public isExpanded: BehaviorSubject<boolean>;

  togglePanel() {
    this.isExpanded.next(!this.isExpanded.value);
  }
}
