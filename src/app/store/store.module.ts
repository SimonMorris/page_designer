import { NgModule } from '@angular/core';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { reducer } from './reducer';
import { dispatchCustomEvent } from '../utils';
import { EActionType, EEventTarget } from '../enums';
import { IViewerState } from '../interfaces';

export function storeLogger(_reducer: ActionReducer<any>): ActionReducer<any> {
  return function(state, action) {
    console.log(action.type);
    return _reducer(state, action);
  };
}

export function sync(_reducer: ActionReducer<any>): ActionReducer<any> {
  return function(state, action) {
    const result = _reducer(state, action);

    if (action.type !== EActionType.SYNC) {
      dispatchCustomEvent<IViewerState>('iframe:sync', result.viewer, EEventTarget.FRAME);
    }
    return result;
  };
}

export const metaReducers: MetaReducer[] = [storeLogger, sync];

@NgModule({
  imports: [StoreModule.forRoot({ viewer: reducer }, { metaReducers })],
  exports: [StoreModule]
})
export class AppStoreModule {}
