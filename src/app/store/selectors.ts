import { createSelector } from '@ngrx/store';
import { IViewerState, State } from '../interfaces';
import { ZoomOptions } from '../registry/zoom_options';
import { EOrientation } from '../enums';

export const selectViewer = (state: State) => state.viewer;

export const zoomLevel = createSelector(
  selectViewer,
  (state: IViewerState) => ZoomOptions[state.zoomOptionIdx]
);

export const scale = createSelector(
  selectViewer,
  (state: IViewerState) => state.scale
);

export const orientation = createSelector(
  selectViewer,
  (state: IViewerState) => state.orientation
);

export const viewerWidth = createSelector(
  selectViewer,
  orientation,
  (state: IViewerState, _orientation) => {
    if (_orientation === EOrientation.PORTRAIT) {
      return Math.min(state.scale.width, state.scale.height);
    } else {
      return Math.max(state.scale.width, state.scale.height);
    }
  }
);

export const scaledWidth = createSelector(
  zoomLevel,
  viewerWidth,
  (_zoom, _width) => _width * _zoom
);

export const viewerHeight = createSelector(
  selectViewer,
  orientation,
  (state: IViewerState, _orientation) => {
    if (_orientation === EOrientation.LANDSCAPE) {
      return Math.min(state.scale.width, state.scale.height);
    } else {
      return Math.max(state.scale.width, state.scale.height);
    }
  }
);

export const scaledHeight = createSelector(
  zoomLevel,
  viewerHeight,
  (_zoom, _height) => _height * _zoom
);
