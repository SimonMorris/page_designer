import { IViewerState } from '../interfaces';
import { DEFAULT_SCAlE_OPTION_KEY, ScaleOptions } from '../registry/scale_options';
import { EOrientation } from '../enums';
import { DEFAULT_ZOOM_IDX } from '../registry/zoom_options';

export const state: IViewerState = {
  zoomOptionIdx: DEFAULT_ZOOM_IDX,
  scale: ScaleOptions[DEFAULT_SCAlE_OPTION_KEY],
  orientation: EOrientation.PORTRAIT,
};
