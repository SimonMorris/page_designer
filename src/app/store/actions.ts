import { createAction, props } from '@ngrx/store';
import { ICanvasScale, IViewerState } from '../interfaces';
import { EActionType, EOrientation } from '../enums';

export const sync = createAction(EActionType.SYNC, props<IViewerState>());

export const zoomIn = createAction(EActionType.ZOOM_IN);
export const zoomOut = createAction(EActionType.ZOOM_OUT);
export const rotate = createAction(EActionType.ROTATE);
export const device = createAction(EActionType.DEVICE_CHANGE, props<{ scale: ICanvasScale, orientation: EOrientation }>());
