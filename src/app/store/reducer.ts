import { Action, createReducer, on } from '@ngrx/store';
import { state } from './state';
import * as AppActions from './actions';
import { clamp } from '../utils';
import { ZoomOptions } from '../registry/zoom_options';
import { IViewerState } from '../interfaces';

const zoomBoundaries = clamp(0, ZoomOptions.length - 1);

const viewerReducer = createReducer(
  state,
  on(AppActions.sync, (_prevState, _newState) => _newState),
  on(AppActions.zoomIn, _state => ({ ..._state, zoomOptionIdx: zoomBoundaries(_state.zoomOptionIdx + 1) })),
  on(AppActions.zoomOut, _state => ({ ..._state, zoomOptionIdx: zoomBoundaries(_state.zoomOptionIdx - 1) })),
  on(AppActions.rotate, _state => ({ ..._state, orientation: (_state.orientation + 1) % 2 })),
  on(AppActions.device, (_state, { scale, orientation }) => ({ ..._state, scale, orientation })),
);

export function reducer(_state: IViewerState | undefined, action: Action) {
  return viewerReducer(_state, action);
}
