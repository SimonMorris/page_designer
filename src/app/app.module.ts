import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routing-module';

import { DragDropModule } from '@angular/cdk/drag-drop';

import { AppComponent } from './app.component';
import { MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { TrustedPipe } from './pipes/trusted.pipe';
import { HtmlstructureComponent } from './ui-components/page-designer/html_structure/htmlstructure/htmlstructure.component';
import { HtmlstructureLineComponent } from './ui-components/page-designer/html_structure/htmlstructure-line/htmlstructure-line.component';
import { HtmlviewerComponent } from './ui-components/page-designer/html_viewer/htmlviewer/htmlviewer.component';
import { HtmlviewerLineComponent } from './ui-components/page-designer/html_viewer/htmlviewer-line/htmlviewer-line.component';
import { BasicEditorComponent } from './ui-components/page-designer/editors/basic-editor/basic-editor.component';
import { FormsModule } from '@angular/forms';
import { CssviewerComponent } from './ui-components/page-designer/css_viewer/cssviewer/cssviewer.component';
import { CssviewerFileComponent } from './ui-components/page-designer/css_viewer/cssviewer-file/cssviewer-file.component';
import { PageDesignerComponent } from './ui-components/page-designer/page-designer.component';
import { WorkspaceComponent } from './ui-components/workspace/workspace.component';
import { ViewerComponent } from './ui-components/viewer/viewer.component';
import { WorkspaceHeaderComponent } from './ui-components/workspace/workspace-header/workspace-header.component';
import { WorkspaceSidebarComponent } from './ui-components/workspace/workspace-sidebar/workspace-sidebar.component';
import { WorkspaceConfiguratorComponent } from './ui-components/workspace/workspace-configurator/workspace-configurator.component';
import { ViewerHeaderComponent } from './ui-components/viewer/viewer-header/viewer-header.component';
import { AppStoreModule } from './store/store.module';
import { ViewerFrameComponent } from './ui-components/viewer/viewer-frame/viewer-frame.component';
import { ViewerOverlayComponent } from './ui-components/viewer/viewer-overlay/viewer-overlay.component';
import { ViewerFooterComponent } from './ui-components/viewer/viewer-footer/viewer-footer.component';
import { ObserversModule } from '@angular/cdk/observers';
import { ViewerHtmlComponent } from './ui-components/viewer/viewer-html/viewer-html.component';
import { FPipe } from './pipes/f.pipe';
import { JoinPipe } from './pipes/join.pipe';
import { PropertyEditorComponent } from './ui-components/workspace/workspace-configurator/property-editor/property-editor.component';
import {
  ViewerHtmlLineSimpleComponent
} from './ui-components/viewer/viewer-html/viewer-html-line-simple/viewer-html-line-simple.component';
import { ViewerHtmlLineFullComponent } from './ui-components/viewer/viewer-html/viewer-html-line-full/viewer-html-line-full.component';
import { HasValuePipe } from './pipes/has-value.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TrustedPipe,
    FPipe,
    HtmlstructureComponent,
    HtmlstructureLineComponent,
    HtmlviewerComponent,
    HtmlviewerLineComponent,
    BasicEditorComponent,
    CssviewerComponent,
    CssviewerFileComponent,
    PageDesignerComponent,
    WorkspaceComponent,
    ViewerComponent,
    WorkspaceHeaderComponent,
    WorkspaceSidebarComponent,
    WorkspaceConfiguratorComponent,
    ViewerHeaderComponent,
    ViewerFrameComponent,
    ViewerOverlayComponent,
    ViewerFooterComponent,
    ViewerHtmlComponent,
    JoinPipe,
    PropertyEditorComponent,
    ViewerHtmlLineSimpleComponent,
    ViewerHtmlLineFullComponent,
    HasValuePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppStoreModule,
    DragDropModule,
    ObserversModule,
    FormsModule,
    MDBBootstrapModulesPro.forRoot(),
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
