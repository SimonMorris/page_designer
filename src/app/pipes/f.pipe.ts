import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'f'
})
export class FPipe implements PipeTransform {
  transform<K, T>(value: K, f: (...params) => T, ...args): T {
    return f(value, ...args);
  }
}
